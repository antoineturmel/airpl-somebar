# airpl.org Somebar
Affiche un indicateur sur la qualité de l'air sur le tableau de bord grâce à [Somebar](https://github.com/limpbrains/somebar)

1. Installer [Somebar](https://github.com/limpbrains/somebar)
2. Copier les fichiers du dossier images dans ~/.somebar
3. Exécuter somebar sur le port 1738
4. Définir les droits de parse.sh avec chmod +x
5. Lancer ./parse.sh
6. ???
7. PROFIT !
