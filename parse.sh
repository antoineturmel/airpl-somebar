#!/bin/bash
DATE=`date +%Y-%m-%d`
wget -O data.json "http://www.airpl.org/api/mesures?polluant=24&zones=2&debut=$DATE&fin=$DATE"
NIVEAU=`cat data.json | jq '.mesures."110".data[0].niveau' -r`
IMAGE="question"
if [ $NIVEAU -le 40 ]; then IMAGE="greencloud";
elif [ $NIVEAU -le 70 ]; then IMAGE="orangecloud";
elif [ $NIVEAU -le 100 ]; then IMAGE="redcloud";
fi
echo -n $IMAGE | nc -4u -w0 localhost 1738
